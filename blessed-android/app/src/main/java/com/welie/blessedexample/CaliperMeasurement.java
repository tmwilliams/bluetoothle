package com.welie.blessedexample;

import com.welie.blessed.BluetoothBytesParser;

import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class CaliperMeasurement implements Serializable {
    public Date timestamp;
    public String value;

    public CaliperMeasurement(String value) {
        this.value = value;
        timestamp = Calendar.getInstance().getTime();
    }

    @Override
    public String toString() {
        DateFormat df = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss", Locale.ENGLISH);
        String formattedTimestamp;
        if(timestamp != null) {
            formattedTimestamp = df.format(timestamp);
        } else {
            formattedTimestamp = "null";
        }
        return String.format(Locale.ENGLISH,"%s at (%s)", value, formattedTimestamp);
    }
}
